//打印100到200之间的素数嵌套练习
//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)//生成100-200的数字
//	{
//		int flag = 1;//用来判断和假设i是素数
//		int n = 0;//用来定义被除数
//		for (n = 2; n <i; n++)//生成2-到-1的数字
//		{
//			if (i % n == 0)//判断是否是素数
//			{
//				flag = 0;//辅助判断、定义
//				break;
//			}
//		}
//		if (flag == 1)//辅助判断
//		{
//			printf("%d", i);
//		}
//	}
//	return 0;
//}

//打印100到200之间的素数嵌套练习优化1
//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)//生成100-200的数字
//	{
//		int flag = 1;//用来判断和假设i是素数
//		int n = 0;//用来定义被除数
//		for (n = 2; n <= sqrt(i); n++)//生成2-到-1的数字
//		{
//			if (i % n == 0)//判断是否是素数
//			{
//				flag = 0;//辅助判断、定义
//				break;
//			}
//		}
//		if (flag == 1)//辅助判断
//		{
//			printf("%d\n", i);
//		}
//	}
//	return 0;
//}

//打印100到200之间的素数嵌套练习优化2
//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//	int i = 0;
//	for (i = 101; i <= 200; i+=2)//生成100-200的数字
//	{
//		int flag = 1;//用来判断和假设i是素数
//		int n = 0;//用来定义被除数
//		for (n = 3; n <= sqrt(i); n++)//生成2-到-1的数字
//		{
//			if (i % n == 0)//判断是否是素数
//			{
//				flag = 0;//辅助判断、定义
//				break;
//			}
//		}
//		if (flag == 1)//辅助判断
//		{
//			printf("%d\n", i);
//		}
//	}
//	return 0;
//}